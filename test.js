'use strict';

var tester = require('greenlock-challenge-test');

var challenger = require('./').create({});

// The dry-run tests can pass on, literally, 'example.com'
// but the integration tests require that you have control over the domain
var domain = 'example.com';
var wildname = '*.example.com';

tester.test('http-01', domain, challenger).then(function () {
  console.info("PASS http-01");
  return tester.test('dns-01', wildname, challenger).then(function () {
    console.info("PASS dns-01");
  });
}).then(function () {
  return tester.test('fake-01', domain, challenger).then(function () {
    console.info("PASS fake-01");
  });
}).catch(function (err) {
  console.error("FAIL");
  console.error(err);
  process.exit(20);
});
