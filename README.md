# [greenlock-challenge-manual](https://git.coolaj86.com/coolaj86/greenlock-challenge-manual.js)

| A [Root](https://rootprojects.org) Project |

An extremely simple reference implementation
of an ACME (Let's Encrypt) challenge strategy
for [Greenlock](https://git.coolaj86.com/coolaj86/greenlock-express.js) v2.7+ (and v3).

* Prints the ACME challenge details to the terminal
  * (waits for you to hit enter before continuing)
* Asks you to enter the challenge response.
* Let's you know it's safe to remove the challenge.

Other ACME Challenge Reference Implementations:

* [**greenlock-challenge-manual**](https://git.coolaj86.com/coolaj86/greenlock-challenge-manual.js)
* [greenlock-challenge-http](https://git.coolaj86.com/coolaj86/greenlock-challenge-http.js)
* [greenlock-challenge-dns](https://git.coolaj86.com/coolaj86/greenlock-challenge-dns.js)

Install
-------

```bash
npm install --save greenlock-challenge-manual@3.x
```

Usage
-----

```bash
var Greenlock = require('greenlock');

Greenlock.create({
  ...
, challenges: { 'http-01': require('greenlock-challenge-manual')
              , 'dns-01': require('greenlock-challenge-manual')
              , 'tls-alpn-01': require('greenlock-challenge-manual')
              }
  ...
});
```

Note: If you request a certificate with 6 domains listed,
it will require 6 individual challenges.

Exposed (Promise) Methods
---------------

For ACME Challenge:

* `set(opts)`
* `remove(opts)`

The options will look like this for normal domains:

```js
{ challenge: {
    type: 'http-01'
  , identifier: { type: 'dns', value: 'example.com' }
  , wildcard: false
  , expires: '2012-01-01T12:00:00.000Z'
  , token: 'abc123'
  , thumbprint: '<<account key thumbprint>>'
  , keyAuthorization: 'abc123.xxxx'
  , dnsHost: '_acme-challenge.example.com'
  , dnsAuthorization: 'yyyy'
  , altname: 'example.com'
  }
}
```

And they'll look like this for wildcard domains:

```js
{ challenge: {
    type: 'http-01'
  , identifier: { type: 'dns', value: 'example.com' }
  , wildcard: true
  , expires: '2012-01-01T12:00:00.000Z'
  , token: 'abc123'
  , thumbprint: '<<account key thumbprint>>'
  , keyAuthorization: 'abc123.xxxx'
  , dnsHost: '_acme-challenge.example.com'
  , dnsAuthorization: 'yyyy'
  , altname: '*.example.com'
  }
}
```

The only difference is that `altname` will have the `*.` prefix (which you would expect
but, of course, can't work as a specific a DNS record) and the `wildcard` property is `true`.

Optional

* `get(limitedOpts)`

Because the get method is apart from the main flow (such as a DNS query),
it's not always implemented and the options are much more limited in scope:

```js
{ challenge: {
    type: 'http-01'
  , identifier: { type: 'dns', value: 'example.com' }
  , wildcard: false
  , token: 'abc123'
  , altname: 'example.com'
  }
}
```

If there were an implementation of Greenlock integrated directly into
a NameServer (which currently there is not), it would probably look like this:

```js
{ challenge: {
    type: 'dns-01'
  , identifier: { type: 'dns', value: 'example.com' }
  , token: 'abc123'
  , dnsHost: '_acme-challenge.example.com'
  }
}
```
